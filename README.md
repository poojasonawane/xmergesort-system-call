CSE 506 OPERATING SYSTEMS HW1
Name: Pooja Sonawane
SBU ID: 110739621

This file describes the usage information of xmergesort system call and its user level invocation which can be done as follows:

	./xmergesort [-uaitd] outfile.txt file1.txt file2.txt

The system calls merges file1 and file2(delimited by a newline character) and produce result in outfile. If outfile already exists, then it is truncated completely. In case of error, no partial results are returned.
Either u or a flag should always be specified. These flags can be used as below:
-u : output sorted records; if duplicates found, output only one copy
-a : output all records, even if there are duplicates
-i : compare records case-insensitive (case sensitive by default)
-t : input should be in sorted order
-d : return the number of sorted records in "data"
-h : displays the help message

data: return the total number of records that were written to output file, subject to the above flags.

The project contains following files:

1. install_module.sh
--------------------
This is a shell script to insert and remove the loadable module.

2. kernel.config
----------------
Configuration file for loading minimum components of the kernel. 

3. Makefile
-----------
This file contains cleaning and building instructions for the project.

4. sys_xmergesort.h
-------------------
This header file defines the struct variable named sysargs which is used for storing all the required parameters of this syscall, which are:
-input_file1
-input_file2
-output_file
-flags
-data 

5. sys_xmergesort.c
-------------------
This file is the implementation of the xmergesort system call, which is linked to the call via function pointer. The system call accepts input from user as a void* and returns zero on success or an error code, otherwise. The implementation steps are as follows:
- First, the input from user is validated and the memory locations pointed by the pointers are verified for proper access.
- Then these validated arguments are copied to kernel space by using copy_from_user into a sysargs struct pointer. This is the data structure which is used in later steps for managing all the user arguments.
- Then, in the next step, the files are opened/created and are validated by doing several checks. Some of these include check for existence of file, checking if any of the given files are same, if the files are opened with proper access.
- After this, iteratively the files are read into buffers and the sorted results are accumulated in a buffer. Once this buffer is full, it is emptied by writing the contents into the output file. This step is continued unless both the files are completely scanned.
- This behaviour is suitable even if the input files are larger than the page size. But, if any record occurs which cannot be fit in the buffer, then appropriate error message is returned.
- In the meanwhile, the sorting is done based on the selection of flags done by the user. If any problem occurs, then appropriate error messages are returned to the user space.
- In case, the t flag is set, then any unsorted inputs are terminated by EINVAL error, otherwise, if t flag is not set, then any unsorted records are skipped while merging the files.
- In the end, the number of sorted records are returned, if d flag is set, otherwise, all the allocated resources are deallocated and the control is returned to userland.
6. xmergesort.c
---------------
This is the user code for invoking the xmergesort system call, with some basic input validations.
