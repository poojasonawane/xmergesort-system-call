#include <asm/unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/syscall.h>
#include <unistd.h>
#include "sys_xmergesort.h"

#ifndef __NR_xmergesort
#error xmergesort system call not defined
#endif

int main(int argc, char *argv[])
{
	int rc=0, flag=0, is_d=0, is_u=0, is_a=0;
	u_int flags = 0x0, data=0;
	sysargs args;
	
	while((flag = getopt(argc, argv, "uaitdh")) != -1){
                switch(flag){
                        case 'u':
				is_u = 1;
				flags = flags | 0x01;
                                break;
                        case 'a':
				is_a = 1;
				flags = flags | 0x02;
                                break;
                        case 'i':
				flags = flags | 0x04;
                                break;
                        case 't':
				flags = flags | 0x10;
                                break;
                        case 'd':
				is_d = 1;
				args.data = &data;
				flags = flags | 0x20;
				break;
			case 'h':
				printf("It is a program to merge-sort two input files and produce result in an output file.\n");
				printf("Usage: ./xmergesort [-uaitd] outfile.txt file1.txt file2.txt\n");
				printf("-u: output sorted records; if duplicates found, output only one copy. Cannot be specified with 'a'\n");
				printf("-a: output all records, even if there are duplicates. Cannot be specified with 'u'\n");
				printf("-i: compare records case-insensitive (case sensitive by default)\n");
				printf("-t: accept input files in sorted order only\n");
				printf("-d: return the number of sorted records\n");
				printf("-h: display help message\n");
				goto out;
                        case '?':
                                printf("Invalid option %c \n",flag);
                                break;
                }
        }

	if(!(is_u ^ is_a)){
		printf("One among u and/or a flag should be specified. Try again or type -h for help.\n");
		goto out;
	}
        args.outfile = (char *)argv[optind];
        args.infile1 = (char *)argv[optind+1];
        args.infile2 = (char *)argv[optind+2];
	args.flags = flags;

	if(!args.infile1 || !args.infile2 || !args.outfile){	
		printf("Error: infile1, infile2 and outfile are mandatory parameters. Try again or type -h for help.\n");
		goto out;
	}
  	rc = syscall(__NR_xmergesort, &args);
	if (rc == 0){
		printf("Success! Syscall returned %d\n", rc);
		if(is_d)
			printf("Number of sorted records produced is: %u\n", *args.data);
	}else{
		printf("Failed! Syscall returned %d (errno=%d)\n", rc, errno);
		perror("Error");
	}

	out:
	exit(rc);
}
