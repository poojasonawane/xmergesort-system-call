#include <linux/linkage.h>
#include <linux/moduleloader.h>
#include <linux/limits.h>
#include <linux/fs.h>
#include <asm/uaccess.h>
#include <linux/fcntl.h>
#include <linux/slab.h>
#include <linux/string.h>
#include "sys_xmergesort.h"
#define BUFFSIZE PAGE_SIZE

u_int UFLAG = 0;
u_int AFLAG = 0;
u_int IFLAG = 0;
u_int TFLAG = 0;
u_int DFLAG = 0;
static char *prev = NULL;

asmlinkage extern long (*sysptr)(void *arg);

//returns status of kth flag bit
unsigned int get_flag(unsigned int flag, unsigned int k) {
    int mask = 1 << k;
    return (flag & mask) >> k;
}

//copies user arguments into kernel space and returns errorno, if error exists
int get_arguments(sysargs *uargs, sysargs *kargs){
	int errno = 0, notcopied = 0;

	//allocate memory for infile1 and copy user arguments into it	
	kargs->infile1 = kmalloc(strlen(uargs->infile1)+1, GFP_KERNEL);
	if(kargs->infile1 == NULL){
		errno = -ENOMEM;
		printk("Failed to allocate memory for kernel arguments.\n");
		goto out;
	}
	memset(kargs->infile1, 0, strlen(uargs->infile1)+1);
	notcopied = copy_from_user(kargs->infile1, uargs->infile1, strlen(uargs->infile1));
	if(notcopied){
		errno = -EBADE;
		printk("Failed to copy infile 1 in kernel space.\n");
		goto out;
	}
		
	//allocate memory for infile2 and copy user arguments into it
	kargs->infile2 = kmalloc(strlen(uargs->infile2)+1, GFP_KERNEL);
        if(kargs->infile2 == NULL){
                errno = -ENOMEM;
                printk("Failed to allocate memory for kernel arguments.\n");
                goto out;
        }
	memset(kargs->infile2, 0, strlen(uargs->infile2)+1);
        notcopied = copy_from_user(kargs->infile2, uargs->infile2, strlen(uargs->infile2));
        if(notcopied){
                errno = -EBADE;
                printk("Failed to copy infile 2 in kernel space.\n");
                goto out;
        }

	//allocate memory for outfile and copy user arguments into it
	kargs->outfile = kmalloc(strlen(uargs->outfile)+1, GFP_KERNEL);
        if(kargs->outfile == NULL){
                errno = -ENOMEM;
                printk("Failed to allocate memory for kernel arguments.\n");
                goto out;
        }
	memset(kargs->outfile, 0, strlen(uargs->outfile)+1);
        notcopied = copy_from_user(kargs->outfile, uargs->outfile, strlen(uargs->outfile));
        if(notcopied){
                errno = -EBADE;
                printk("Failed to copy outfile in kernel space.\n");
                goto out;
        }

	//copy flags into kernel space
	notcopied = copy_from_user(&kargs->flags, &uargs->flags, sizeof(u_int));
	if(notcopied){
		errno = -EBADE;
		printk("Failed to copy flags in kernel space.\n");
		goto out;
	}

	//initialize the flag variables
	UFLAG = get_flag(kargs->flags, 0);
        AFLAG = get_flag(kargs->flags, 1);
        IFLAG = get_flag(kargs->flags, 2);
        TFLAG = get_flag(kargs->flags, 4);
        DFLAG = get_flag(kargs->flags, 5);

	//check either u or a flag is selected
	if(!(UFLAG ^ AFLAG)){
                printk("One among 'u' and/or 'a' flag should be specified.\n");
                errno = -EINVAL;
                goto out;
        }

	//allocate memory for 'data' if data flag is set
	if(DFLAG){
		kargs->data = (u_int *)kmalloc(sizeof(u_int), GFP_KERNEL);
		if(kargs->data == NULL){
			errno = -ENOMEM;
			printk("Failed to allocate memory for kernel arguments.\n");
			goto out;
		}
		memset(kargs->data, 0, sizeof(u_int));
	}
	
	return errno;

	out:
	//check and free all the allocated variables, in case of error
	if(kargs->infile1 != NULL){
		kfree(kargs->infile1);
	}
	if(kargs->infile2 != NULL){
                kfree(kargs->infile2);  
        }
	if(kargs->outfile != NULL){
                kfree(kargs->outfile);  
        }
	if(kargs->data != NULL){
		kfree(kargs->data);
	}
	return errno;
}

//validates arguments and returns errno, if error exists
int validate_inputs(sysargs *args){
        int errno = 0;
	
	//check if name of all the files(infile1, infile2, and outfile) is within the max filename limit
        if(args->infile1 == NULL || strlen(args->infile1) > NAME_MAX){
                printk("Infile1 is invalid.\n");
                errno = args->infile1 == NULL ? -EINVAL : -ENAMETOOLONG;
                goto out;
        }
        if(args->infile2 == NULL || strlen(args->infile2) > NAME_MAX){
                printk("Infile2 is invalid.\n");
                errno = args->infile2 == NULL ? -EINVAL : -ENAMETOOLONG;
                goto out;
        }
        if(args->outfile == NULL || strlen(args->outfile) > NAME_MAX){
                printk("Outfile is invalid.\n");
                errno = args->outfile == NULL ? -EINVAL : -ENAMETOOLONG;
                goto out;
        }

	//verify permissions on all memory locations
        if( !access_ok(VERIFY_READ, args->infile1,0 )){
                printk("Bad address for infile1.\n");
                errno = -EFAULT;
                goto out;
		}
        if( !access_ok(VERIFY_READ, args->infile2,0 )){
                printk("Bad address for infile2.\n");
                errno = -EFAULT;
                goto out;
        }
        if( !access_ok(VERIFY_READ, args->outfile,0 )){
                printk("Bad address for outfile.\n");
                errno = -EFAULT;
                goto out;
        }

        out:
        return errno;
}

//returns comparison value of two strings based on the flags set (case sensitive/insensitive) 
int get_comparison(char *s1, char *s2){
	if(!IFLAG)
		return(strcmp(s1,s2));
	else
		return(strcasecmp(s1,s2));
}

//removes partial lines from buffer and returns the new position of file pointer to be set for next read
int remove_partial(char **rbuff, int readf){
	char *temp= *rbuff;
        int i, count = 0;
	for(i=readf-1; i>=0; i--){
		if(temp[i] == '\n' || temp[i] == '\0'){
			temp[i]='\0';
			break;
		}
		count++;
	}
	return (count == readf) ? -1 : count;  
}

//refills the buffer from the file and returns the number of bytes read
int fill_buffer(struct file *infile, char **rbuff){
	mm_segment_t oldfs;
	int readf, count;
	
	//free old buffer and allocate memory for new buffer	
	kfree(*rbuff);
	*rbuff = (char *)kmalloc(BUFFSIZE, GFP_KERNEL);	
	memset(*rbuff,0, BUFFSIZE+1);

	oldfs=get_fs();
	set_fs(KERNEL_DS);
	readf = vfs_read(infile, *rbuff, BUFFSIZE, &infile->f_pos); 
	set_fs(oldfs);

	//file end reached
	if(!readf){
		*rbuff = NULL;
		goto out;
	}
	count = remove_partial(rbuff, readf);

	//file record is larger than read buffer size
	if(count == -1){
		readf = -1;
		goto out;
	}
	infile->f_pos -= count;
	readf -= count;
        (*rbuff)[infile->f_pos] = '\0';
	
	out:
	return readf;
}

//writes a line to buffer and empties the buffer by writing it to output file, if buffer is full. Returns error according to flag specification by user
int write_buffer(struct file *outfile, char **wbuff, size_t *wbytes, char *line, int flush_buffer, u_int *total_written){
	mm_segment_t oldfs;
	int errno = 0;
	int result;
	if(prev != NULL && line != NULL) {
		result = get_comparison(prev,line);
		
		//if not in sorted order then skip record if !t flag else return error
		if( result>0){
			errno = TFLAG ? -EINVAL : 0;
			if(errno)
				printk("Input is not sorted\n");
			goto out;
		}
		
		//skip equal records if u flag is set
		if( !result && UFLAG)
			goto out;
	}
	if(flush_buffer || ((strlen(line) + *wbytes) > BUFFSIZE)){
		oldfs = get_fs();
		set_fs(KERNEL_DS);
		vfs_write(outfile, *wbuff, *wbytes, &outfile->f_pos);
		set_fs(oldfs);
		*wbytes = 0;
		
		kfree(*wbuff);
		*wbuff = kmalloc(BUFFSIZE, GFP_KERNEL);
		memset(*wbuff, 0, BUFFSIZE+1);
		
		if(flush_buffer){
			prev = NULL;
			goto out;
		}
	}
	memcpy((*wbuff)+(*wbytes), line, strlen(line));
        *wbytes = strlen(line) + *wbytes;
	memcpy((*wbuff)+(*wbytes), "\n", 1);
        *wbytes = *wbytes + 1;
	*total_written = *total_written + 1;
	
	prev = line;
	
	out:
	return errno;
}

//returns errno if files are same
int check_files(struct file *file1, struct file *file2, struct file *file3){
	int errno = 0;
	
	//check file1 and file2
	if(file1->f_path.dentry->d_inode->i_sb == file2->f_path.dentry->d_inode->i_sb && file1->f_path.dentry->d_inode->i_ino == file2->f_path.dentry->d_inode->i_ino){
		errno = -EINVAL;
		goto out;
	}

	//check file2 and file3
	if(file2->f_path.dentry->d_inode->i_sb == file3->f_path.dentry->d_inode->i_sb && file2->f_path.dentry->d_inode->i_ino == file3->f_path.dentry->d_inode->i_ino){
                errno = -EINVAL;
                goto out;
        }
	
	//check file1 and file3
	if(file1->f_path.dentry->d_inode->i_sb == file3->f_path.dentry->d_inode->i_sb && file1->f_path.dentry->d_inode->i_ino == file3->f_path.dentry->d_inode->i_ino){
                errno = -EINVAL;
                goto out;
        }

	out:
	return errno;
}

//unlink file
int delete_file(struct file *given_file){
	int errno = 0;
	struct inode *file_inode = given_file->f_path.dentry->d_parent->d_inode;
	struct dentry *file_dentry = given_file->f_path.dentry;
	
	//close file before unlinking
	filp_close(given_file, NULL);
	errno = vfs_unlink(file_inode, file_dentry, NULL);
	
	return errno;
}

//rename file
int rename_file(struct file *file1, struct file *file2){
	int errno = 0;
	struct inode *old_dir = file1->f_path.dentry->d_parent->d_inode;
	struct dentry *old_dentry = file1->f_path.dentry;
	struct inode *new_dir = file2->f_path.dentry->d_parent->d_inode;
	struct dentry *new_dentry = file2->f_path.dentry;
	
	errno = vfs_rename(old_dir, old_dentry, new_dir, new_dentry, NULL, 0);

	return errno;
}

//merges two files and returns errno if error exists
int merge_files(char *infilename1, char *infilename2, char *outfilename, u_int *total_written){
        int cmp_result = 0;
	size_t readf1 = 0, readf2 = 0, wbytes = 0;
        struct file *infile1 = NULL, *infile2 = NULL , *outfile = NULL, *outfile_renamed = NULL;
        char *rbuff1 = NULL, *rbuff2 = NULL, *wbuff = NULL; 
	char *line1 = NULL, *line2 = NULL;
	int errno = 0;
	u_int delete_partial = 0;

	//open files and return error if file does not exist
	infile1 = filp_open(infilename1, O_RDONLY, 644);
        if(!infile1 || IS_ERR(infile1)){
                printk("Error in opening infile1\n");
                errno = (int)PTR_ERR(infile1);
		infile1 = NULL;
                goto out_fileclose;
        }

        infile2 = filp_open(infilename2, O_RDONLY, 644);
        if(!infile2 || IS_ERR(infile2)){
                printk("Error in opening infile2\n");
                errno = (int)PTR_ERR(infile2);
		infile2 = NULL;
                goto out_fileclose;
        }

        outfile_renamed = filp_open(outfilename, O_WRONLY|O_CREAT, 0);
        if(!outfile_renamed || IS_ERR(outfile_renamed)){
                printk("Error in opening outfile\n");
                errno = (int)PTR_ERR(outfile_renamed);
		outfile_renamed = NULL;
                goto out_fileclose;
        }
	
	//this is temporary output file
	outfile = filp_open("temp_file.txt", O_WRONLY|O_CREAT|O_TRUNC, 0);
        if(!outfile || IS_ERR(outfile)){
                printk("Error in opening tempfile\n");
                errno = (int)PTR_ERR(outfile);
                outfile = NULL;
                goto out_fileclose;
        }


	//check files are regular
	if(!S_ISREG(infile1->f_inode->i_mode) || !S_ISREG(infile2->f_inode->i_mode) || !S_ISREG(outfile_renamed->f_inode->i_mode)){
		printk("File should be a regular file.\n");
		errno = -EBADF;
		goto out_fileclose;
	}

	//check files are different
	errno = check_files(infile1,infile2,outfile_renamed);
	if(errno){
		printk("Files cannot be same.\n");
		goto out_fileclose;
	}

	//assigning permissions to output file, no more permissive than first input file
	outfile->f_path.dentry->d_inode->i_mode = infile1->f_path.dentry->d_inode->i_mode;
	outfile->f_path.dentry->d_inode->i_opflags = infile1->f_path.dentry->d_inode->i_opflags;
	outfile->f_path.dentry->d_inode->i_uid = infile1->f_path.dentry->d_inode->i_uid;
	outfile->f_path.dentry->d_inode->i_gid = infile1->f_path.dentry->d_inode->i_gid;
	outfile->f_path.dentry->d_inode->i_flags = infile1->f_path.dentry->d_inode->i_flags;
	
	//initialize file pointers in all files
        infile1->f_pos = 0;
        infile2->f_pos = 0;
        outfile->f_pos = 0;

	rbuff1 = (char *) kmalloc(BUFFSIZE, GFP_KERNEL);
        if (!rbuff1){
                printk("rbuff1 kmalloc failed.\n");
		errno = -ENOMEM;
                goto out;
        }
	memset(rbuff1, 0, BUFFSIZE+1);

	rbuff2 = (char *) kmalloc(BUFFSIZE, GFP_KERNEL);
        if (!rbuff2){
                printk("rbuff2 kmalloc failed.\n");
		errno = -ENOMEM;
                goto out;
        }
	memset(rbuff2, 0, BUFFSIZE+1);

	wbuff = (char *) kmalloc(BUFFSIZE, GFP_KERNEL);
        if (!wbuff){
                printk("wbuff kmalloc failed.\n");
                errno = -ENOMEM;
		goto out;
        }
	memset(wbuff, 0, BUFFSIZE+1);
	
	readf1 = fill_buffer(infile1, &rbuff1);
	if(readf1==-1){
		delete_partial = 1;
		printk("File record larger than buffer size.\n");
		errno = -EFBIG;
		goto out;
	}
	readf2 = fill_buffer(infile2, &rbuff2);
	if(readf2==-1){
                delete_partial = 1;
                printk("File record larger than buffer size.\n");
                errno = -EFBIG;
                goto out;
        }
	line1 = strsep(&rbuff1, "\n");
        line2 = strsep(&rbuff2, "\n");
	
	while((readf1 && readf2)){
		printk("Start loop -------line1: %s, line2: %s, rbuff1:%s , rbuff2: %s\n", line1, line2, rbuff1, rbuff2);
		if(line1 == NULL || line2 == NULL){
			break;
		}
		cmp_result = get_comparison(line1,line2);
		if(cmp_result<0){
			printk("line1 is smaller: %s\n",line1);
			errno = write_buffer(outfile, &wbuff, &wbytes, line1, 0, total_written);
			if(errno){
                                delete_partial = 1;
                                goto out;
                        }
			line1 = strsep(&rbuff1, "\n");
                        
			printk("after cmp line1: %s buff %s\n",line1, rbuff1);
			if(!line1){
				readf1 = fill_buffer(infile1, &rbuff1);			
				if(readf1==-1){
			                delete_partial = 1;
                			printk("File record larger than buffer size.\n");
                			errno = -EFBIG;
                			goto out;
        			}
				printk("Rbuff refilled Read from f1: %zd bytes, buff: %s, size: \n",readf1, rbuff1);
				line1 = strsep(&rbuff1, "\n");
			}
		}else{
                        printk("line2 is smaller: %s\n",line2);
			errno = write_buffer(outfile, &wbuff, &wbytes, line2, 0, total_written);
                        if(errno){
				delete_partial = 1;
				goto out;
			}
			line2 = strsep(&rbuff2, "\n");

                        printk("After cmp line2: %s buff: %s\n",line2,rbuff2);
			if(!line2){
				readf2 = fill_buffer(infile2, &rbuff2);   
                                if(readf2==-1){
			                delete_partial = 1;
                			printk("File record larger than buffer size.\n");
                			errno = -EFBIG;
                			goto out;
        			}
				printk("Rbuff2 refilled Read from f2: %zd bytes, buff: %s, size: \n",readf2, rbuff2);
				line2 = strsep(&rbuff2, "\n");
                        }
                }

		printk("Ending while loop itr -----line1: %s, line2: %s, rbuff1: %s, rbuff2: %s\n", line1, line2, rbuff1, rbuff2);
	}
	while(readf1){
		printk("line1 is : %s\n",line1);
		errno = write_buffer(outfile, &wbuff, &wbytes, line1, 0, total_written);
		if(errno){
                        delete_partial = 1;
			goto out;
                }
		line1 = strsep(&rbuff1, "\n");
		if(!line1){
			readf1 = fill_buffer(infile1, &rbuff1);
			if(readf1==-1){
		                delete_partial = 1;
                		printk("File record larger than buffer size.\n");
                		errno = -EFBIG;
                		goto out;
        		}
			printk("Read from f1: %zd bytes, buff: %s\n",readf1, rbuff1);
			line1 = strsep(&rbuff1, "\n");
		}
	}
	while(readf2){
                printk("line2 is : %s\n",line2);
		errno = write_buffer(outfile, &wbuff, &wbytes, line2, 0, total_written);
		if(errno){
                	delete_partial = 1;
                        goto out;
                }
		line2 = strsep(&rbuff2, "\n");
                if(!line2){
                        readf2 = fill_buffer(infile2, &rbuff2);
                        if(readf2==-1){
		                delete_partial = 1;
                		printk("File record larger than buffer size.\n");
                		errno = -EFBIG;
                		goto out;
        		}
                        line1 = strsep(&rbuff2, "\n");
                }
        }
	
	errno = write_buffer(outfile, &wbuff, &wbytes, NULL, 1, total_written);
	if(errno){
        	delete_partial = 1;
                goto out;
        }
	printk("Total records in merge_files: %u\n",*total_written);
	
	out:
	if(rbuff1 != NULL){
                kfree(rbuff1);
        }
        if(rbuff2 != NULL){
                kfree(rbuff2);
        }
        if(wbuff != NULL){
                kfree(wbuff);
        }
	//if error occurred, delete temp file, else rename it
	if(delete_partial){
		printk("Error in merge-sort. Deleting partial output file.\n");
		delete_file(outfile);
		outfile = NULL; //file closed in delete_file
	}
	else{
		rename_file(outfile, outfile_renamed);
	}
	//closing and unlinking outfile
	//delete_file(outfile);
	//outfile = NULL;

	out_fileclose:
	if(infile1 != NULL){
        	filp_close(infile1,NULL);
        }
	if(infile2 != NULL){
		filp_close(infile2, NULL);
        }
	if(outfile_renamed != NULL){
		filp_close(outfile_renamed, NULL);
	}
	if(outfile != NULL){
                filp_close(outfile,NULL);
        }

	return errno;
}

asmlinkage long xmergesort(void *arg){
	int errno = 0, notcopied;
	u_int data = 0;
	sysargs *kargs = NULL;
	sysargs *uargs = (sysargs *)arg;
	
	//validate user arguments
	errno = validate_inputs(uargs);
        if(errno)
                goto out;
	
	//allocate memory for kernel arguments
	kargs = (sysargs *)kmalloc(sizeof(sysargs), GFP_KERNEL);
	if(kargs == NULL){
		errno = -ENOMEM;
		printk("Failed to allocate memory for kernel arguments.\n");
		goto out;
	}
	
	//get user arguments into kernel space
	errno = get_arguments(uargs, kargs);
	if(errno)
		goto out;
	printk("xmergesort received arguments: flags: u:%u a:%u i:%u t:%u d:%u, in1: %s, in2: %s, out: %s\n",UFLAG, AFLAG, IFLAG, TFLAG, DFLAG, kargs->infile1, kargs->infile2, kargs->outfile);	
	
	//merge infile 1 and infile2 into outfile
	errno = merge_files(kargs->infile1, kargs->infile2, kargs->outfile, &data);
	if(errno)
		goto out;
	
	//if d flag is set, then copy number of records written into data field in userspace
	if(DFLAG){
		*kargs->data = data;
		notcopied = copy_to_user(uargs->data, kargs->data, sizeof(u_int));
		if(notcopied){
			printk("Failed to copy %d bytes. Try again.\n",notcopied);
			errno = -EAGAIN;
			goto out;
		}
	}
		
	out:	
	printk("\n----END OF SYSCALL----\n"); 

	//reset static pointer prev
	prev = NULL;

	//deallocate memory
	if(kargs->infile1 != NULL){
                kfree(kargs->infile1);
        }
        if(kargs->infile2 != NULL){
                kfree(kargs->infile2);
        }
        if(kargs->outfile != NULL){
                kfree(kargs->outfile);
        }
        if(kargs->data != NULL){
                kfree(kargs->data);
        }
	if(kargs){
		kfree(kargs);
		kargs = NULL;
	}		
	return errno;
}

static int __init init_sys_xmergesort(void)
{
	printk("installed new sys_xmergesort module\n");
	if (sysptr == NULL)
		sysptr = xmergesort;
	return 0;
}
static void  __exit exit_sys_xmergesort(void)
{
	if (sysptr != NULL)
		sysptr = NULL;
	printk("removed sys_xmergesort module\n");
}
module_init(init_sys_xmergesort);
module_exit(exit_sys_xmergesort);
MODULE_LICENSE("GPL");
MODULE_AUTHOR("Pooja Sonawane");
MODULE_DESCRIPTION("System call for merging two files in sorted order");
